/******************************************************************************
Need to have a code which can store N numbers and when isSum(value) called -  need to 

check if “Value” is sum of any two numbers previously stored

*******************************************************************************/
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

public class Main
{
 public static void main (String[]args)
  {
   TwoSum twoSum = new TwoSumImpl ();
   twoSum.store (5);
   twoSum.store (6);
   boolean result1 = twoSum.isSum (5);
   boolean result2 = twoSum.isSum (10);
   boolean result3 = twoSum.isSum (11);

   System.out.println ("Result1..." + result1);
   System.out.println ("Result2..." + result2);
   System.out.println ("Result3..." + result3);
  }
}

class TwoSumImpl implements TwoSum
{
 private final HashSet < Integer > hs = new HashSet < Integer > ();
 public void store (int value)
  {
    hs.add (new Integer (value));
  }
 public boolean isSum (int value)
  {
   int len = hs.size ();
   final List < Integer > list = new ArrayList < Integer > ();
   list.addAll (hs);
   boolean flag = false;
 do
 {
  final int content = list.get (--len);
  final int res = value - content;
   if (res != content)
	  {
     if (hs.contains (res))
       {
        flag = true;
        break;
       }else
	      {
		flag = false;
   }
 }
}while (len > 0);
 return flag;
 }
}

/**
 * Defines an interface for detecting if a number is the sum of two previously stored 
   numbers.
 */

interface TwoSum
{
   /**
     * Store the input value for later use in isSum
     * @param value the value that needs to be stored
     */
 public void store (int value);
    /**
     * Determine if value is the sum of exactly two values that have been passed to the 
       store function
     * @param value the value that we want to test
     * @return true if value is the sum of two values passed to the store function
     * false if it is not the sum of two values
     */
  public boolean isSum (int value);
}
