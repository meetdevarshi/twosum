class Solution1 {
    public int solution(int[] A) {
        // write your code in Java SE 8
        int val=0;
        ArrayList<Integer> al = new ArrayList<Integer>();
        for (int i=0;i<A.length;i++){
         al.add(A[i]);   
        }
        Collections.sort(al,Collections.reverseOrder());
        for (int i=0;i<A.length;i++){
        int length = (int) (Math.log10(al.get(i)) + 1);
        if (length==3){
         val=al.get(i).intValue();
         break;
         }
        }
    return val;
    }

}