class Solution{

    String stringWithOutSpaces = S.replace(" ", "-");
    String stringWithOutSeparator[] = stringWithOutSpaces.split("-");

    String fullString = "";
    for (int i = 0; i < stringWithOutSeparator.length; i++) {
      fullString += stringWithOutSeparator[i];
    }

    int INCR = 3;
    int startIndex = 0;
    int fullStringLength = fullString.length();
    String result = "";
    for (int i = 0; i < fullStringLength; i++) {
      int endIndex = startIndex + INCR;
      String subString = "";
      if (endIndex < fullStringLength - 1) {
        subString = fullString.substring(startIndex, endIndex);
        String subStringLeft = fullString.substring(endIndex, fullStringLength);
        int size = subStringLeft.length();
        int rest = size - INCR;
        int div = size / INCR;
        if (div < 2 && rest < 2) {
          INCR = 2;
        }
        result += subString + "-";
        startIndex = endIndex;
      } else {
        endIndex = fullStringLength;
        subString = fullString.substring(startIndex, endIndex);
        result += subString;
        break;
      }

    }

    return result;
  }