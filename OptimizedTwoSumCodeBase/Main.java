/******************************************************************************
Need to have a code which can store N numbers and when isSum(value) called -  need to 

check if “Value” is sum of any two numbers previously stored

*******************************************************************************/
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.lang.Integer;
import java.util.Optional;
import java.util.Enumeration;
public class Main
{
 public static void main (String[]args)
  {
   TwoSum twoSum = new TwoSumImpl();
   twoSum.store (5);
   twoSum.store (6);
   boolean result1 = twoSum.isSum (5);
   boolean result2 = twoSum.isSum (10);
   boolean result3 = twoSum.isSum (11);

   System.out.println ("Result1..." + result1);
   System.out.println ("Result2..." + result2);
   System.out.println ("Result3..." + result3);
  }
}

class TwoSumImpl implements TwoSum
{
 private int lstscdval=0;
 private int result=0;
 private static int counter=0;
 
 private final Hashtable <Integer,Integer> ht = new Hashtable <Integer,Integer> ();
 private int content = 0;
 public void store (int value)
  {
    Optional<Hashtable> optionalHT = Optional.ofNullable(ht); 
    Integer key = new Integer (value);
    ht.put(key,new Integer (value));
    if(optionalHT.isPresent()){
     counter= ht.size();  
    }
    Enumeration e = ht.keys();
    Integer tempkey = (Integer) e.nextElement();
    int prevKey= tempkey.intValue();
    --prevKey;
    if(optionalHT.isPresent()){
      Integer retrieveKey = new Integer(prevKey);    
    Integer temp=(Integer)ht.get(retrieveKey); 
    Optional<Integer> optionalInteger = Optional.ofNullable(temp);
    if(optionalInteger.isPresent()){
      lstscdval=temp.intValue();
    }
    result = lstscdval + value;
  }

 }

 public boolean isSum (int value1)
  {
    boolean flag = false;
    int value2=value1-lstscdval; 
    Integer find = new Integer(value2);
    if (ht.contains(find) && find!=lstscdval){
        flag = true;
        }else{
	flag = false;
    }
    return flag;
  }
}


/**
 * Defines an interface for detecting if a number is the sum of two previously stored 
   numbers.
 */

interface TwoSum
{
   /**
     * Store the input value for later use in isSum
     * @param value the value that needs to be stored
     */
 public void store (int value);
    /**
     * Determine if value is the sum of exactly two values that have been passed to the 
       store function
     * @param value the value that we want to test
     * @return true if value is the sum of two values passed to the store function
     * false if it is not the sum of two values
     */
  public boolean isSum (int value);
}
